"use strict";
const path = require("path");
module.exports = {
    publicPath: process.env.NODE_ENV !== "production" ? '/' : '/home',
    assetsDir: 'static',
    runtimeCompiler: true,
    transpileDependencies: ["vuetify"],
    outputDir: path.resolve(__dirname, "../server/views/home"),
    lintOnSave: false,
    css: {
        // Enable CSS source maps.
        sourceMap: process.env.NODE_ENV !== "production",
    },
    devServer: {
        port: 8080,
        open: true, // 自动打开浏览器
        proxy: {
            "/api": {
                target: "http://localhost:9090",
                secure: false,
                changeOrigin: true,
                pathRewrite: {
                    "^/api": "/api",
                },
            },
            "/config": {
                target: "http://localhost:9090",
                secure: false,
                changeOrigin: true,
                pathRewrite: {
                    "^/config": "/config",
                },
            },
            "/upload": {
                target: "http://localhost:9090",
                secure: false,
                changeOrigin: true,
                pathRewrite: {
                    "^/upload": "/upload",
                },
            },
            "/tmp": {
                target: "http://localhost:9090",
                secure: false,
                changeOrigin: true,
                pathRewrite: {
                    "^/tmp": "/tmp",
                },
            },
        },
    },
};
