/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [],
    variants: {
        extend: {
            visibility: ["hover", "focus", "group-hover"],
            divideColor: ["group-hover"],
            borderRadius: ['hover', 'focus'],
            borderStyle: ['hover', 'focus']
        },
    },
    theme: {
        extend: {},
    },
    plugins: [],
};
