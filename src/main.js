import Cloud from "./cloud-vue";
import App from './App.vue'
import element from "./plugins/element";
import router from "@/routers";
new Cloud({
    router,
    element,
    render: h => h(App),
})
