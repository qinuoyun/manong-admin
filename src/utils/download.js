

/**
 * ======================================
 * download
 * author: sky-sky
 * file: download.js
 * date: 2023/09/27 11:22
 * ======================================
 */
export function downloadContacts(){
    console.log("下载通讯录导入模板")
    window.open('/download/通讯录导入模板.xlsx')
}

export function formatFileSize(size) {
    if (size === 0) return '0 B';
    if(!size)return '--'
    const units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const k = 1024;
    const i = Math.floor(Math.log(size) / Math.log(k));
    return parseFloat((size / Math.pow(k, i)).toFixed(2)) + ' ' + units[i];
}

const files = require.context("../assets/file-icon", true);
const list = {}
files.keys().forEach((key) => {
    let fileArray = key.split("/");
    let name = fileArray[1]
    let type = name.split('.')[0]
    list[type] = require("../assets/file-icon/" + name)
})
export function getFileTypeIcon(type) {
    if(list[type]){
        return list[type]
    }else{
        return list['file']
    }
}

export function download(url,name = 'file'){
    let link = document.createElement('a');
    link.href = url;
    link.download = name;
    link.click();
}
