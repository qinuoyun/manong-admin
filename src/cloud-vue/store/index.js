import store from "./store";
import {getToken, setToken, setRefreshToken, clearStorage} from "../utils/auth";

const getDefaultState = () => {
    return {
        token: getToken(),
        isLogin: !!getToken(),
        version: "v1.0.0",
        userInfo: {}
    }
}

const state = getDefaultState()

const mutations = {
    SET_TOKEN: (state, {token, refreshToken}) => {
        setToken(token)
        setRefreshToken(refreshToken)
        state.token = token
    },
    SET_USERINFO: (state, data) => {
        state.userInfo = data
    }
}


const actions = {
    /**
     * 执行用户登录
     * @param commit
     * @param fun
     * @param userInfo
     * @returns {Promise<unknown>}
     */
    login({commit}, fun) {
        return new Promise((resolve, reject) => {
            fun().then(data => {
                let {token, refreshToken} = data;
                commit("SET_TOKEN", {token, refreshToken})
                commit("SET_USERINFO", data)
                resolve(data)
            }).catch(error => {
                reject(error)
            })
        });
    },
    logout() {
        return new Promise((resolve, reject) => {
            try {
                clearStorage();
                resolve(true)
            } catch (error) {
                reject(error)
            }
        });
    }
};

const storeName = "cloud_application";

export function install() {
    // 判断是否注册了模型
    if (!store.hasModule(storeName)) {
        store.registerModule([storeName], {
            namespaced: true,
            state,
            mutations,
            actions,
            getters: {
                getState: (state) => (value) => {
                    return state[value];
                },
                isLogin: () => () => {
                    return !!getToken()
                }
            }
        });
    }
}

export function uninstall() {
    if (store.hasModule(storeName)) {
        store.unregisterModule(storeName)
    }
}

export function getStore() {
    //执行注册
    install();
    return {
        state: store.state[storeName],
        commit(type, payload, options) {
            type = storeName + '/' + type
            return store.commit(type, payload, options)
        },
        dispatch(type, payload) {
            type = storeName + '/' + type
            return store.dispatch(type, payload)
        },
        getter(value) {
            let type = storeName + '/getState';
            return store.getters[type](value)
        },
        isLogin() {
            let type = storeName + '/isLogin';
            return store.getters[type]();
        }
    }
}