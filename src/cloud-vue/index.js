import Vue from "vue";
import router from "./router";
import request from './request'
import {getStore} from "./store"
import {getToken, setToken} from "./utils/auth";


export default class cloud {
    static router = null;

    constructor(object) {
        Vue.prototype.$cloud = cloud;
        object['router'] = cloud.router = router(object.router);

        return new Vue(object).$mount('#app');
    }

    /**
     * 获取Token
     * @param value
     * @returns {*}
     */
    static token(value = null) {
        if (value) {
            setToken(value)
        } else {
            return getToken()
        }
    }


    static prefix() {
        return {};
    }

    /**
     * 数据GET方法
     * @param url
     * @param params
     * @returns {*}
     */
    static get(url, params = null) {
        if (url.indexOf("/") === 0) {
            url = url.slice(1)
        }
        //重置URL地址
        url = `/api/` + url;
        return request({
            url: url,
            method: 'GET',
            headers: {
                'x-api-type': 'app'
            },
            params: Object.assign(cloud.prefix(), params),
        })
    }

    /**
     * 创建POST
     * @param url
     * @param params
     * @param data
     * @returns {*}
     */
    static post(url, params = null, data = null) {
        if (url.indexOf("/") === 0) {
            url = url.slice(1)
        }
        //设置数据结构
        if (data === null) {
            data = params;
            params = null
        }
        //重置URL地址
        url = `/api/` + url;
        return request({
            url: url,
            method: 'POST',
            headers: {
                'x-api-type': 'app'
            },
            params: Object.assign(cloud.prefix(), params),
            data: data
        })
    }

    /**
     * 判断是否登录
     * @returns {*}
     */
    static isLogin() {
        return getStore().isLogin()
    }

    /**
     * 登录方法
     */
    static login() {
        return new Promise((resolve, reject) => {
            getStore().dispatch('login', () => {
                return cloud.post.apply(cloud, Array.from(arguments))
            }).then(data => {
                resolve(data)
            }).catch(error => {
                reject(error);
            })
        })
    }

    static logout() {
        return new Promise((resolve, reject) => {
            getStore().dispatch('logout').then(data => {
                resolve(data)
            }).catch(error => {
                reject(error);
            })
        })
    }

    /**
     * 跳转
     */
    static go(url, query = null) {
        if (Object.prototype.toString.call(url) === "[object Object]") {
            if (url.query) {
                url.query = Object.assign(url.query, query);
            }
        } else {
            url = {
                path: url,
                query
            }
        }
        cloud.router.push(url);
    }
}