import Cookies from "js-cookie";

const TokenKey = "qinuouyn_cloud_template_token";
const RefreshKey = "qinuouyn_cloud_refresh_token";
const locationKey = "qinuouyn_cloud_refresh_token";

export function getToken() {
    return Cookies.get(TokenKey);
}

export function setToken(token) {
    return Cookies.set(TokenKey, token);
}

export function setRefreshToken(token) {
    return Cookies.set(RefreshKey, token);
}

export function getRefreshToken() {
    return Cookies.get(RefreshKey);
}

export function removeToken() {
    return Cookies.remove(TokenKey);
}


export function clearStorage() {
    Cookies.remove(TokenKey);
    Cookies.remove(locationKey);
    Cookies.remove(RefreshKey);
}

export function getLocation() {
    let data = Cookies.get(locationKey);
    if (data) {
        try {
            return JSON.parse(data);
        } catch (e) {
            return null;
        }
    }
    return null;
}

export function setLocation(data) {
    return Cookies.set(locationKey, JSON.stringify(data));
}

export function removeLocation() {
    return Cookies.remove(locationKey);
}
