//VUE的JS库
import Vue from 'vue'
import VueRouter from 'vue-router'
import NProgress from "nprogress"; // progress bar
import "nprogress/nprogress.css"; // progress bar style
import {getStore} from "../store"

Vue.use(VueRouter)

/**
 * 返回路由信息
 * @param routes
 * @param whiteList
 * @returns {VueRouter}
 */
export default function (routes, whiteList = ["/", "/pages/login/login"]) {
    let router = new VueRouter({
        routes // (缩写) 相当于 routes: routes
    })
    /**
     * 路由前置方法
     */
    router.beforeEach(async (to, from, next) => {
        //启动页面进度条
        NProgress.start();
        //用于存储路由数据
        if (to.params && Object.keys(to.params).length) {
            localStorage.setItem("routerParams", JSON.stringify(to.params));
        }
        const Store = getStore();
        //白名单问题
        if (whiteList.indexOf(to.path) !== -1) {
            next();
        } else {
            if (Store.isLogin()) {
                if (to.path === "/pages/login/login") {
                    next({path: "/"});
                } else {
                    next();
                }
                console.log("DAU卡撒娇大豪科技")
            } else {
                if (to.path === "/pages/login/login") {
                    next();
                } else {
                    next(`/pages/login/login?redirect=${to.path}`);
                }
            }
        }
    });

    /**
     * 路由后置方法
     */
    router.afterEach(() => {
        // finish progress bar
        NProgress.done();
    });

    return router;
}



