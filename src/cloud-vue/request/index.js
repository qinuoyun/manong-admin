import axios from 'axios'
import application from '@/config.js'
import {
    getToken,
    setToken,
    getRefreshToken,
    setRefreshToken
} from '../utils/auth'
import {Message} from "element-ui";


const instance = axios.create({
    withCredentials: true,
    crossDomain: true,
})

// 请求拦截
instance.interceptors.request.use(
    config => {
        let token = getToken();
        //设置请求头
        if (token) {
            config.headers['Authorization'] = 'Bearer ' + token;
        }
        let refreshToken = getRefreshToken();
        //设置请求头
        if (refreshToken) {
            config.headers['RefreshToken'] = refreshToken;
        }
        console.log("发送的请求信息", config)
        //返回配置信息
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

// 响应拦截
instance.interceptors.response.use(
    response => {
        console.log(`[DEBUG:请求成功「${new Date()}」]`, response)
        if (response.status >= 200 && response.status < 300) {
            if (response.data.code === 0) {
                //用于Token存储
                if (response.data.token) {
                    setToken(response.data.token);
                }
                //用于RefreshToken存储
                if (response.data.refreshToken) {
                    setRefreshToken(response.data.refreshToken);
                }
                return Promise.resolve(response.data.data)
            } else {
                //处理40000-50000自动报错
                if (response.data.code >= 40000 && response.data.code < 50000) {

                }
                return Promise.reject(response.data)
            }
        }
        return Promise.reject(response)
    },
    error => {
        console.error(`[DEBUG:请求报错「${new Date()}」]`, error)
        if (error.response) {
            let {status, data} = error.response;
            if (status > 300) {
                if (data && data.code) {
                    Message({
                        message: `[${data.code}]-${data.msg}`,
                        type: 'warning'
                    })
                    //此处需要自动报错
                    return Promise.reject(data);
                }
            }
            return Promise.reject(error.response);
        }
        return Promise.reject(error);
    }
);


export default instance