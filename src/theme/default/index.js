import AdminLayout from "./AdminLayout.vue"
import EmptyLayout from "./EmptyLayout.vue"
const components = [
    AdminLayout,
    EmptyLayout
];
const install = function (Vue) {
    components.forEach(component => {
        window[component.name] = component;
        Vue.component(component.name, component);
    });
}

export default {
    version: '3.0.0',
    install,
}