const {EmptyLayout} = window;
export default [
    {
        path: "/pages/panel/panel",
        name: "center-panel",
        meta: {
            icon: "el-icon-ali-dingyuehao",
            title: "首页概览",
        },
        component: () => import("../pages/panel/panel.vue"),
    },
    {
        path: "/pages/category/category",
        name: "category",
        meta: {
            icon: "el-icon-eleme",
            title: "分类管理",
        },
        component: () => import("@/pages/category/category.vue")
    },
    {
        path: "/pages/resource/resource",
        name: "resource",
        meta: {
            icon: "el-icon-eleme",
            title: "资源管理",
        },
        component: EmptyLayout,
        children: [
            {
                path: "/pages/resource/videos",
                name: "videos",
                meta: {

                    title: "视频",
                },
                component: () => import("@/pages/resource/videos.vue")
            }, {
                path: "/pages/student/images",
                name: "images",
                meta: {

                    title: "图片",
                },
                component: () => import("@/pages/resource/images.vue")
            }, {
                path: "/pages/student/courseware",
                name: "courseware",
                meta: {

                    title: "课件",
                },
                component: () => import("@/pages/resource/courseware.vue")
            }
        ]
    },
    {
        path: "/pages/student/student",
        name: "student",
        meta: {
            icon: "el-icon-eleme",
            title: "学员管理",
        },
        component: () => import("@/pages/student/student.vue")
    },
    {
        path: "/pages/setting/setting",
        name: "setting",
        meta: {
            icon: "el-icon-eleme",
            title: "系统设置",
        },
        component: () => import("@/pages/setting/setting.vue")
    }
]