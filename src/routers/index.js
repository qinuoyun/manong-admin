
import admin from "./admin.js";
// 每个路由应该映射一个组件。 其中"component" 可以是
// 通过 Vue.extend() 创建的组件构造器，
// 或者，只是一个组件配置对象。
// 我们晚点再讨论嵌套路由。
const {AdminLayout} = window;
const routes = [
    {
        path: '/',
        name: "home",
        component: AdminLayout,
        redirect: "/pages/panel/panel",
        children:admin
    },
    {
        path: "/pages/login/login",
        name: "login",
        component: () => import("@/pages/login/login.vue")
    }
]


export default routes;
