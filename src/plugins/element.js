import Vue from "vue";
import ElementUI from "element-ui";
import Theme from "@/theme/default"
import "@/theme/default/styles/index.scss";
import "./tailwind.css";
import "./icon/index.css"

Vue.use(ElementUI);
Vue.use(Theme)

export default ElementUI;
